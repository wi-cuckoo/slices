// +build ignore,OMIT

package main

import (
	"crypto/tls"
	"fmt"

	"github.com/go-gomail/gomail" // HL
)

func main() {
	d := gomail.NewDialer("smtp.exmail.qq.com", 586, "example@qq.com", "123456")
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true} // bypass verification
	m := gomail.NewMessage()
	m.SetHeader("From", "example@qq.com")
	m.SetHeader("To", "liuwei@163.com")
	m.SetHeader("Subject", "Hello!")
	m.SetBody("text/html", "Hello <b>Bob</b> and <i>Cora</i>!")

	s, err := d.Dial()
	if err != nil {
		panic(err.Error()) // HL
	}
	if err := gomail.Send(s, m); err != nil {
		fmt.Println("send err:", err.Error())
	}
}

// END OMIT

// func main() {
// 	d := gomail.NewDialer("smtp.exmail.qq.com", 586, "example@qq.com", "123456")
// 	d.TLSConfig = &tls.Config{InsecureSkipVerify: true} // bypass verification
// 	m := gomail.NewMessage()
// 	m.SetHeader("From", "example@qq.com")
// 	m.SetHeader("To", "liuwei@163.com")
// 	m.SetHeader("Subject", "Hello!")
// 	m.SetBody("text/html", "Hello <b>Bob</b> and <i>Cora</i>!")

// 	errCh := make(chan error)
// 	go func() {
// 		s, err := d.Dial()
// 		if err != nil {
// 			errCh <- err
// 			return
// 		}
// 		errCh <- gomail.Send(s, m)
// 	}()
// 	select {
// 	case e := <-errCh:
// 		fmt.Println("send mail err:", e)
// 	case <-time.After(time.Second): // if 20s
// 		fmt.Println("send mail timeout")
// 	}
// }
