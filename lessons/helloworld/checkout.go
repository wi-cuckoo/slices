// +build ignore,OMIT

package main

import (
	"flag"
	"fmt"
	"time"

	"gitlab.com/wi-cuckoo/slices/lessons/common/util" // HL
)

var start string
var h bool

func init() {
	flag.StringVar(&start, "s", "10:00", "time on check-in, eg. 10:00")
	flag.BoolVar(&h, "h", false, "print help")
}

func main() {
	flag.Parse()
	if h {
		flag.Usage()
		return
	}

	end, err := util.Checkout(start, time.Minute*570) // 上班9.5h // HL
	if err != nil {
		fmt.Printf("出错了：%s\n", err.Error())
		return
	}
	fmt.Println("今天下班时间是：", end)
}

// END OMIT
