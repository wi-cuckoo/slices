package main

import (
	"fmt"
	"os"
)

func main() {
	fmt.Print("不换行")
	fmt.Println("hello go")
	fmt.Fprintln(os.Stdout, "os.Stdout: hello go")
	fmt.Fprintln(os.Stderr, "os.Stderr: hello go")
}
