// +build ignore,OMIT

package main

import (
	"fmt"
	"time"
)

var msgCh = make(chan string)

func main() {
	go boring("boring!")
	for i := 0; i < 10; i++ { // if i < 6 instead, what will happend
		fmt.Println("Got msg:", <-msgCh) // HL
	}
	fmt.Println("you'r so boring, I'm leaving!")
}

func boring(msg string) {
	for i := 0; i < 10; i++ {
		msgCh <- fmt.Sprintf("%s %d", msg, i) // HL
		time.Sleep(time.Millisecond * 100)
	}
}

// STOP OMIT
