// +build ignore,OMIT

package main

import (
	"fmt"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().Unix())
}

// START OMIT
func main() {
	quit := make(chan bool)
	go boring("Joe", quit)
	go boring("Ann", quit)
	go boring("Sam", quit)
	time.Sleep(time.Second)
	close(quit)             // HL
	time.Sleep(time.Second) // 再续一秒哈
}

func boring(msg string, quit chan bool) {
	for i := 0; ; i++ {
		select {
		case _, ok := <-quit: // HL
			if !ok {
				fmt.Println(msg, "got quit signal, closing...")
				return
			}
		default:
			fmt.Printf("%s %d\n", msg, i)
			time.Sleep(time.Millisecond * time.Duration(rand.Intn(100)))
		}
	}
}

// STOP OMIT
