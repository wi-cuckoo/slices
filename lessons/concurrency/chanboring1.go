// +build ignore,OMIT

package main

import (
	"fmt"
	"time"
)

func main() {
	ch := boring("boring!")
	for i := 0; i < 10; i++ {
		fmt.Println("Got msg:", <-ch) // HL
	}
	fmt.Println("you'r so boring, I'm leaving!")
}

func boring(msg string) <-chan string {
	msgCh := make(chan string)
	go func() {
		for i := 0; ; i++ {
			msgCh <- fmt.Sprintf("%s %d", msg, i) // HL
			time.Sleep(time.Millisecond * 100)
		}
	}()
	return msgCh
}

// STOP OMIT
