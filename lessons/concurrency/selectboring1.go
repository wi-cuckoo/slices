// +build ignore,OMIT

package main

import (
	"fmt"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().Unix())
}

// START OMIT
func main() {
	msgCh := boring("Joe")
	for {
		select {
		case s := <-msgCh:
			fmt.Println("Got msg:", s)
		case <-time.After(time.Millisecond * 500): // potential memory leak // HL
			fmt.Println("you're too slow, exit!")
			return
		}
	}
}

func boring(msg string) <-chan string {
	msgCh := make(chan string)
	go func() {
		for i := 0; ; i++ { // HL
			msgCh <- fmt.Sprintf("%s %d", msg, i)
			time.Sleep(time.Millisecond * time.Duration(rand.Intn(1000))) // HL
		}
	}()
	return msgCh
}

// STOP OMIT
