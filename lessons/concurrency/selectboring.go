// +build ignore,OMIT

package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	msgCh := fanin(boring("Joe"), boring("Ann"))
	for i := 0; i < 10; i++ {
		fmt.Println("Got msg:", <-msgCh) // HL
	}
	fmt.Println("you'r so boring, I'm leaving!")
}

// START1 OMIT
func fanin(in1, in2 <-chan string) <-chan string {
	out := make(chan string)
	go func() {
		for {
			select {
			case val := <-in1:
				out <- val
			case val := <-in2:
				out <- val
			}
		}
	}()
	return out
}

// STOP1 OMIT

func boring(msg string) <-chan string {
	msgCh := make(chan string)
	go func() {
		for i := 0; i < 10; i++ {
			msgCh <- fmt.Sprintf("%s %d", msg, i) // HL
			time.Sleep(time.Millisecond * time.Duration(rand.Intn(100)))
		}
	}()
	return msgCh
}

// STOP OMIT
