// +build ignore,OMIT

package main

import (
	"fmt"
	"math/rand"
	"time"
)

// START1 OMIT
func main() {
	msgCh := fanin(boring("Joe"), boring("Ann"))
	for i := 0; i < 10; i++ { // if i < 6 instead, what will happend
		fmt.Println("Got msg:", <-msgCh)
	}
	fmt.Println("you'r so boring, I'm leaving!")
}

func fanin(in1, in2 <-chan string) <-chan string {
	out := make(chan string)
	go func() { for { out <- <-in1 } }() // HL
	go func() { for { out <- <-in2 } }() // HL
	return out
}
// STOP1 OMIT

func boring(msg string) <-chan string {
	msgCh := make(chan string)
	go func() {
		for i := 0; i < 10; i++ {
			msgCh <- fmt.Sprintf("%s %d", msg, i) // HL
			time.Sleep(time.Millisecond * time.Duration(rand.Intn(100)))
		}
	}()
	return msgCh
}

// STOP OMIT
