// +build ignore,OMIT

package main

import (
	"fmt"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().Unix())
}

// START OMIT
func main() {
	close := make(chan string)
	msgCh := boring("Joe", close)
	for i := 0; i < 6; i++ {
		fmt.Println("Got msg:", <-msgCh)
	}
	close <- "bye"                    // HL
	fmt.Println("wait joe:", <-close) // HL
}

func boring(msg string, close chan string) <-chan string {
	msgCh := make(chan string)
	go func() {
		for i := 0; ; i++ {
			select {
			case msgCh <- fmt.Sprintf("%s %d", msg, i):
				time.Sleep(time.Millisecond * time.Duration(rand.Intn(100)))
			case <-close: // HL
				fmt.Println("got close signal, closing...")
				close <- "yeah, bye"
				return
			}
		}
	}()
	return msgCh
}

// STOP OMIT
