// +build ignore,OMIT

package main

import (
	"strconv"
	"time"
)

// START OMIT
var cache = make(map[string]int)

func get(key string) int {
	return cache[key]
}

func set(key string, val int) {
	cache[key] = val
}

func main() {
	for i := 0; i < 100; i++ {
		go set(strconv.Itoa(i), i)    // HL
		go get(strconv.Itoa(100 - i)) // HL
	}
	<-time.After(time.Second) // 续一秒
}

// STOP OMIT
/*
// 不一定会出现，如果在单核机器上百分百不会出现
PS E:\Workspace> go run .\lessons\code\datarace\main.go
fatal error: concurrent map read and map write
*/
