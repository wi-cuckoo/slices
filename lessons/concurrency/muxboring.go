// +build ignore,OMIT

package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

var count int
var mux sync.Mutex

func main() {
	go boring("boring!") // HL
	for {
		mux.Lock()
		c := count
		mux.Unlock()
		runtime.Gosched() // 让出时间片
		if c == 10 {
			fmt.Println("you'r so boring, I'm leaving!")
			break
		}
	}
}

// STOP OMIT
func boring(msg string) {
	for i := 0; i < 10; i++ {
		fmt.Println(msg, i)
		time.Sleep(time.Millisecond * 100)
		mux.Lock()
		count++
		mux.Unlock()
	}
}

// END OMIT
