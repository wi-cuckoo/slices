// +build ignore,OMIT

package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func init() {
	rand.Seed(time.Now().Unix())
}

// START OMIT
func main() {
	quit := make(chan bool)
	var wg = new(sync.WaitGroup) // 如果是 sync.WaitGroup 类型会死锁 // HL
	wg.Add(3)                    // HL
	go boring("Joe", quit, wg)
	go boring("Ann", quit, wg)
	go boring("Sam", quit, wg)
	time.Sleep(time.Millisecond * 200)
	close(quit)
	wg.Wait() // HL
}

func boring(msg string, quit chan bool, wg *sync.WaitGroup) {
	defer wg.Done() // HL
	for i := 0; ; i++ {
		select {
		case _, ok := <-quit:
			if !ok {
				fmt.Println(msg, "got quit signal, closing...")
				return
			}
		default:
			fmt.Printf("%s %d\n", msg, i)
			time.Sleep(time.Millisecond * time.Duration(rand.Intn(100)))
		}
	}
}

// STOP OMIT
