// +build ignore,OMIT

package main

import (
	"log"
	"net"
	"net/http"
)

func main() {
	ln, err := net.Listen("tcp", ":50556")
	if err != nil {
		panic(err.Error())
	}
	http.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("pong\n"))
	})
	log.Fatal(http.Serve(ln, nil))
}
