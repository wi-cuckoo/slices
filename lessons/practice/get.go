// +build ignore,OMIT

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	res, err := http.Get("http://localhost:50556/ping")
	if err != nil {
		panic(err.Error())
	}
	defer res.Body.Close()
	buf, _ := ioutil.ReadAll(res.Body)
	fmt.Println(string(buf))
}
