package main

import (
	"log"
	"net"
	"net/http"
	"sync"
)

func main() {
	ln, err := net.Listen("tcp", ":50556")
	if err != nil {
		panic(err.Error())
	}
	mux := &mySrvMux{
		handlers: make(map[string]http.Handler),
	}
	mux.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("pong\n"))
	})
	log.Fatal(http.Serve(ln, mux)) // HL
}

type mySrvMux struct {
	mux      sync.RWMutex
	handlers map[string]http.Handler
}

func (m *mySrvMux) ServeHTTP(w http.ResponseWriter, r *http.Request) { // HL
	m.mux.RLock()
	defer m.mux.RUnlock()
	h, ok := m.handlers[r.URL.Path]
	if !ok {
		w.WriteHeader(404)
		w.Write([]byte("你搞清楚自己的定位没有\n"))
		return
	}
	h.ServeHTTP(w, r)
}

func (m *mySrvMux) HandleFunc(path string, h func(http.ResponseWriter, *http.Request)) { // HL
	m.mux.Lock()
	defer m.mux.Unlock()
	if _, ok := m.handlers[path]; ok {
		panic("hander registrated for many times")
	}
	m.handlers[path] = http.HandlerFunc(h)
}
