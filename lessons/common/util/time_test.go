package util_test

import (
	"testing"
	"time"

	"gitlab.com/wi-cuckoo/slices/lessons/common/util"
)

func TestCheckout(t *testing.T) {
	if _, err := util.Checkout("", time.Second); err == nil {
		t.Error("empty string can't be parsed")
	}
	if end, err := util.Checkout("10:30", time.Hour*8); err != nil || end != "18:30" {
		t.Error("10:30 add 8 hours should be equal to 18:30, not", end)
	}
}
