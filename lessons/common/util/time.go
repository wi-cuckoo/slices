package util

import (
	"time"
)

// START OMIT

// Checkout to cacl the off time by start time and work duration
func Checkout(from string, d time.Duration) (string, error) {
	// tips: layout must be 2006-01-02 15:04:05
	start, err := time.Parse("15:04", from)
	if err != nil {
		return "", err
	}
	end := start.Add(d)
	return end.Format("15:04"), nil
}

// END OMIT

// var ch = make(chan string)

// func Event(w http.ResponseWriter, r *http.Request) {
// 	flusher, _ := w.(http.Flusher)
// 	io.WriteString(w, ":ping\n\n")
// 	flusher.Flush()

// 	for id := 0; ; id++ {
// 		select {
// 		case <-time.After(time.Second * 10):
// 			io.WriteString(w, ": ping\n\n")
// 			flusher.Flush()
// 		case d, ok := <-ch:
// 			if !ok {
// 				return
// 			}
// 			io.WriteString(w, fmt.Sprintf("data: %s\n\n", d))
// 			flusher.Flush()
// 		}
// 	}
// }
