module gitlab.com/wi-cuckoo/slices

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/go-gomail/gomail v0.0.0-20160411212932-81ebce5c23df
	github.com/jinzhu/gorm v1.9.8 // indirect
	golang.org/x/tools v0.0.0-20190614205625-5aca471b1d59
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df // indirect
)
